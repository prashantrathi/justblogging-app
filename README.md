# Just Blogging
Just Blogging, is my attempt to create a very simple web app with client and server for Full Stack Development.
## Client End: 
React with Redux Store
## Server End: 
Node using Express and MongoDB

> **Expected: node and npm is available on system.**

## Steps to run the app
Open terminal and follow following steps:
1. git clone https://bitbucket.org/prashantrathi/justblogging-app

2. cd justblogging-app

###### Below will install required packages at server end.
3. npm install 
4. cd client/blog-app

###### Below will install required packages at client end.
5. npm install
6. npm run build (this will create production ready client setup)

> As told above this app uses Mongo DB. To talk with MongoDB, the server code uses Mongoose ORM. But setting up
 Mongo DB is another step to run the server with Mongo DB support

#### Depending upon platform follow the steps at: https://docs.mongodb.com/manual/administration/install-community/
6. Once setup is done, **run ./mongod**  at terminal to start mongo db.

7. cd ../../ (come back to project root folder justblogging-app)

8. node app.js

9. If setup above is done successfully then  **Blog Solution started listening.** should be shown on terminal.

10. Go to browser and run localhost:3090

## TODO: 

1) Addition of express sanitizer to avoid illegal scripting in blogs

2) More elegant UI

3) Updating README.md with details about each packages used at both client and server end.

4) Steps to run development client and server at different ports.

5) Refactor to make code more modular.


