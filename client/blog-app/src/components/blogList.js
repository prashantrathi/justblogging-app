import React, { Component } from 'react';
import { fetchBlogList } from '../actions/index';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Container, Image,List } from 'semantic-ui-react';
import NavBar from './navbar';
import '../style/bloglist.css';
import { Link } from 'react-router-dom';


class BlogList extends Component {
	
	componentDidMount() {
		this.props.fetchBlogList();
	}

	renderBlogList() {
		
		const bList = _.map(this.props.blogs, (blog)=>{
						
			return (
				<List.Item key={blog._id} className="blog-list-item">
					<Image avatar src={blog.image} />
					<List.Content>
						<List.Header>{blog.title}</List.Header>
						<List.Description>{blog.created}</List.Description>
					
						<Link to={{pathname : `/blogs/${blog._id}`,
								   state: blog}}>Read More</Link>
						
					</List.Content>

				</List.Item>
				);
		});

		if(bList.length) {
			return bList;
		} else {
			return (
				<div>
					No blogs to list. Click New Post to add new blog post.
				</div> 
			);
		}
		
	}

	render() {
		
		return (
			<div>
				<NavBar />
				<Container className="header-margin">
					<List>
						{this.renderBlogList()}
					</List>
				</Container>
				
			</div>
			);
	}
		
}

function mapStateToProps(state) {
	return { blogs: state.blogs };
}

export default connect(mapStateToProps, {fetchBlogList})(BlogList);