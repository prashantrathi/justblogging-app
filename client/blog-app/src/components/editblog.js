import React, { Component } from 'react';
import { Container, Header, Form, Input, TextArea, Button  } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { submitEditPost } from '../actions/index';
import { Link } from 'react-router-dom';


class EditBlog extends Component {

	handleSubmit = (e)=> {
		 e.preventDefault();
		 		
		const editedBlog = {
			title: e.target.title.value,
			image: e.target.image.value,
			description: e.target.textarea.value
		};

		this.props.submitEditPost(this.props.location.state._id, editedBlog, (error)=>{
			if(!error) {
				this.props.history.push("/blogs/list");
			}
			else {
				console.log("Some Error Happened. Retry!");
			}
		});
	}
	
	render() {
		let {title, image, description, _id} = this.props.location.state;
		
		return (
				<div>
					<Container className="header-margin">
					<Header size="large" textAlign="center">Edit Post</Header>
					
					<Form onSubmit={this.handleSubmit.bind(this)}>
						<Form.Field control={Input} label='Title' name='title' defaultValue={title} />
						<Form.Field control={Input}  label='Image' name='image' defaultValue={ image } />
						<Form.Field control={TextArea}  autoHeight={true} label='Blog Content' name='textarea' defaultValue={ description } />

						<Form.Group>
						<Form.Field control={Button} color="violet" size="big" basic={true} >Submit</Form.Field>
						<Button color="violet" size="big" basic={true} as={Link} to={{
							pathname: `/blogs/${_id}`,
							state: this.props.location.state
						}}>Cancel</Button>
						</Form.Group>
						
					</Form>
					</Container>
				</div>
			);
	}
}

export default connect(null, { submitEditPost })(EditBlog);