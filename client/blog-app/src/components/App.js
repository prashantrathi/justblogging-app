import React, { Component } from 'react';
import '../style/App.css';
import { Link } from 'react-router-dom';

class App extends Component {

  // componentDidMount() {
  //  // this.props.history.push('/blogs');
  // }

  render() {
    
  //  return null;
    
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          <Link to="/blogs/list"> Blogs </Link>
        </p>
      </div>
    );
  }
}

export default App;
