import React, { Component } from 'react';
import NavBar from './navbar';
import { Container, Header, Form, Input, TextArea, Button  } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { submitNewPost } from '../actions/index';

class NewBlogPostForm extends Component {
	
	handleSubmit = (e)=> {
		e.preventDefault();
		
		this.props.submitNewPost({
			title: e.target.title.value,
			image: e.target.image.value,
			description: e.target.textarea.value
		}, (error)=>{
			if(!error) {
				this.props.history.push("/blogs");
			}
			else {
				e.target.title.value = '';
				e.target.image.value = '';
				e.target.textarea.value = '';
				console.log("Some Error Happened. Retry!");
			}
		});

		//console.log(e.target.title.value);
	}
	
	render() {

		return (
				<div>
					<NavBar />
					<Container className="header-margin">
					<Header size="large" textAlign="center">New Post</Header>
					
					<Form onSubmit={this.handleSubmit}>
						<Form.Field control={Input} label='Title' name='title' placeholder='blog title' />
						<Form.Field control={Input} label='Image' name='image' placeholder='image url' />
						<Form.Field control={TextArea} autoHeight={true} label='Blog Content' name='textarea' placeholder='describe your blog content' />
						<Form.Field control={Button} color="violet" basic={true} size="big">Submit</Form.Field>
					</Form>
					</Container>
				</div>
			);
	}
}

export default connect(null, { submitNewPost })(NewBlogPostForm);