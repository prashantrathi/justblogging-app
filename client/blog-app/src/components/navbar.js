import React, { Component } from 'react';
import '../style/navbar.css';
import { Container, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class NavBar extends Component {
	render() {
		return (
			<div>
				<Menu fixed='top' inverted>
			    		<Container>
			        	<Menu.Item as={Link} to="/" header>
			          		<i className="write icon"></i>
			            	Just Blogging
			        	</Menu.Item>
						<Menu.Item as={Link} to="/blogs/list">Home</Menu.Item>
						<Menu.Item as={Link} to="/blogs/new">New Post</Menu.Item>
						</Container>
				</Menu>	
			</div>
			);
	}
}