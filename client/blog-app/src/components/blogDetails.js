import React, { Component } from 'react';
import { Container, Segment, Header, Image, Divider, Button } from 'semantic-ui-react';
import Markdown from 'markdown-to-jsx';
import NavBar from './navbar';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { deletePost } from '../actions/index';


class BlogDetails extends Component {

	// constructor(props) {
	// 	super(props);
	// }

	handleDeletePost(e) {
		e.preventDefault();

		if(window.confirm("Are you sure! This will delete post data permanently.")) {
			console.log(this.props.location.state._id);
			this.props.deletePost(this.props.location.state._id, (error)=>{
				if(!error) {
					this.props.history.push("/blogs/list");
				} else {
					console.log("Faled To Delete");
					this.props.history.push("/blogs/list");
				}
			});

		} else{
			return;
		}

	}

	renderImageUrl(image) {
		if(!image) {
			return;
		} else {
			return (
				<div>
				<Image rounded={true} bordered={true} size='large' centered={true} src={image} />
				</div>
			);
		}
	}

	render() {

		const { title, image, description, _id } = this.props.location.state;
		return (
			<div>
				<NavBar />
				<Container className="header-margin">
					<Segment compact={true} color='blue'>
						<Header as="h2" textAlign='center'>{ title }</Header>
						{this.renderImageUrl(image)}
						<Divider />
						<Markdown>
						{description}
						</Markdown>
					</Segment>
					
					<Button color='violet'  basic={true} size="big" as={Link} to={{
						pathname: `/blogs/${_id}/edit`,
						state: this.props.location.state
					}}>Edit Blog</Button>
					
					<Button color='violet' basic={true} size="big" onClick={this.handleDeletePost.bind(this)}>Delete Blog</Button>
										
				</Container>
			</div>
		);
	}
}
	

export default connect(null, { deletePost })(BlogDetails);

