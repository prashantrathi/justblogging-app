import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';

import './style/index.css';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
// import registerServiceWorker from './registerServiceWorker';
import rootReducer from './reducers';

import App from './components/App';
import BlogList from './components/blogList';
import NewBlogPostForm from './components/newBlogPostForm';
import BlogDetails from './components/blogDetails';
import EditBlog from './components/editblog';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);


ReactDOM.render((
	<Provider store={createStoreWithMiddleware(rootReducer)}>
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={App} />
				<Route exact path="/blogs/list" component={BlogList} />
				<Route exact path="/blogs/new" component={NewBlogPostForm} />
				<Route exact path="/blogs/:id" component={BlogDetails} />
				<Route exact path="/blogs/:id/edit" component={EditBlog} />
			</Switch>
		</BrowserRouter>
	 </Provider>
	), 
	document.getElementById('root'));
//registerServiceWorker();
