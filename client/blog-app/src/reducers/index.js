import { combineReducers } from 'redux';
import BlogsReducer from './bloglist_reducer';

const rootReducer = combineReducers({
	blogs: BlogsReducer
});

export default rootReducer;