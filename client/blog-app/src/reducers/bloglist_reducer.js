import {
	BLOG_LIST
	
} from '../actions/types';
import _ from 'lodash';

// case NEW_POST:
// 			return

export default function(state={}, action) {
	
	switch(action.type) {

		case BLOG_LIST:
			return _.mapKeys(action.payload.data, '_id');



		default:
			return state;
	}

}