import {
	BLOG_LIST,
	NEW_POST,
	EDIT_POST,
	DELETE_POST
	} from './types';
import axios from 'axios';


const BASEURL = "http://localhost:3090";

// /blogs : Restful Index route to fetch all blogs 
export function fetchBlogList() {
	const request = axios.get(`${BASEURL}/blogs`);

	return {
		type: BLOG_LIST,
		payload: request
	}
}

// newPost: an object containing user entered blog post data
// export function submitNewPost(newPost) {
// 	console.log(newPost.title);
// 	const request = axios.post(`${BASEURL}/blogs`, newPost);

// 	return {
// 		type: NEW_POST,
// 		payload: request
// 	}
// }


// newPost: an object containing user entered blog post data
export function submitNewPost(newPost, callback) {

	axios.post(`${BASEURL}/blogs`, newPost)
	.then(response => {
		console.log("New Post Success");
		callback(false);
	})
	.catch(error => {
		console.log("New Post Failure");
		callback(true);
	});
	
	return {
		type: NEW_POST
	}
}

//update route on server is PUT to find and update given blog id
export function submitEditPost(id, newPost, callback) {

	axios.put(`${BASEURL}/blogs/${id}`, newPost)
	.then(response => {
		console.log("Edit Success");
		callback(false);
	})
	.catch(error => {
		console.log("Edit Failure");
		callback(true);
	});
	
	return {
		type: EDIT_POST
	}
}


export function deletePost(id, callback) {
	console.log(id);
	axios.delete(`${BASEURL}/blogs/${id}`)
	.then(response => {
		console.log("Delete Success");
		callback(false);
	})
	.catch(error => {
		console.log("Delete Failed");
		callback(true);
	});

	return {
		type: DELETE_POST
	}
}