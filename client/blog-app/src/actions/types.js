export const BLOG_LIST = "blog_list";
export const NEW_POST = "new_post";
export const EDIT_POST = "edit_post";
export const DELETE_POST = "delete_post";