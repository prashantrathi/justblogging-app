const 	
 		bodyParser 	= 	require('body-parser');	// Used to parse form data in a request submitted using forms.
 		mongoose 	= 	require('mongoose');
 		path		= 	require('path');
 		express 	= 	require('express');
 		cors		=   require('cors');
		app 		= 	express();
		DIST_DIR 	=	path.join(__dirname, "client/blog-app/build");

// APP Middlewares
app.use(express.static(DIST_DIR));
//app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors());

mongoose.connect("mongodb://localhost/blog_solution");

// MONGOOSE MODEL CONFIG
const blogSchema = new mongoose.Schema({
	title: String,
	image: String,
	description: String,
	created: {type: Date, default: Date.now}
});

const Blog = mongoose.model("Blog", blogSchema);


// *********************RESTful Routes***********************


// NEW ROUTE
// Note: This route is to create the form for the user to 
// provide blog data. We have UI in React so this route is handled with UI layer to 
// display the form and take user data. 

// Below route is not called from the client.
// Below route can be commented
// app.get("/blogs/new", (req, res)=> {
// 	console.log(req.body);
// });

// CREATE ROUTE
app.post("/blogs", (req, res)=> {
	const request = req.body;
	
	Blog.create(request, (err, newBlog)=>{
		if(err) {
			console.log("New Blog Create Error!: ", err);
		} else {
			res.send(newBlog);
		}
	});
});


//EDIT ROUTE
// /blogs/:id/edit -> edit route is to show the form with filled
// selected blog post data. This form will allow user to edit the blog post
// This is handled at React client itself. Once user will submit edited post 
// this will be updated in UPDATE Route below.


//UPDATE ROUTE
app.put("/blogs/:id", (req, res)=>{

	const request = req.body;

	Blog.findByIdAndUpdate(req.params.id, req.body, (err, editedBlog)=>{
		if(err) {
			console.log("Editing Blog Error!: ", err);
		} else {
			res.send(editedBlog);
		}
	});
});

//DELETE ROUTE
app.delete("/blogs/:id", (req, res)=> {
	console.log(req.params.id);
	Blog.findByIdAndRemove(req.params.id, (err)=> {

		if(err) {
			console.log("ERROR!, Deletion Failed");
		}
		else {
			res.send("success");
		}
		
	});
});



// INDEX ROUTE
app.get("/blogs", (req, res)=> {

	Blog.find({}, (err, blogs)=> {
		if(err) {
			console.log("ERROR!", err);
		} else {
			res.send(blogs);
		}
	});
});

// The server to catch all requests in case user does refresh
// on pages routed at client end.
app.get("*", (req,res)=> {
	res.sendFile(path.resolve(DIST_DIR, "index.html"));
});



// PORT configuration to start the server
const port = process.env.PORT || 3090;
app.listen(port, process.env.IP, ()=>{
	console.log("Blog Solution started listening.");
});